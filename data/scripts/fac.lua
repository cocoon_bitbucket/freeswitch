--
-- fac.lua  handle bluebox feature access code activation ans deactivation
---

-- get value from arguments
fac= argv[1]
number= argv[2]

--- +CFA: *21      +CFB: *22     +CFNA: *23
--- -CFA: *211*    -CFB: *221*   -CFNA: *231*

--- +CFA:    *2011    +CFB:    *2021   +CFNA:    *2031   +CFNR:    *2041
--- -CFA:    *2010    -CFB:    *2020   -CFNA:    *2030   -CFNR:    *2040

--- +CFA_VM: *2111    +CFB_VM: *2121   +CFNA_VM: *2131   +CFNR_VM: *2141

--- +CFS: *2071 


if number== nil then number = "" end


fac_indicator= string.sub(fac,1,2)   -- *2
fac_function=  string.sub(fac,3,4)   -- 00 to 99
fac_action=    string.sub(fac,5,5)   -- 0 to 9   1=activte, 0=deactivate

-- store info in /bluebox/account/$account/CFA : the number
--            or /bluebox/account/$account/CFB : the number

CFA="01"
CFB="02"
CFNA= "03"
CFNR="04"



-- get value from subscriber
subscriber_number = session:getVariable("accountcode") -- or session:getVariable("caller_id_number"); 
-- session:getVariable("caller_id_number"); 



-- constants
--bluebox_base= "+331465"


-- number parts  +331465  + enterprise(1) + site (1) + value3)
--  eg           +331465       1              1     101      => + 33 1 46 51 1101
-- eg2           +331465       2              2     202      => + 33 1 46 52 2201

subscriber_enterprise= string.sub(subscriber_number,8,8)
subscriber_site= string.sub(subscriber_number,9,9)
subscriber_ident= string.sub(subscriber_number,10,12)
subscriber_extension = string.sub(subscriber_number,9,12)

subscriber_base_number= string.sub(subscriber_number,1,8)

freeswitch.consoleLog("INFO", "bluebox call from "..subscriber_number.." to "..fac..tostring(number).."\n")
freeswitch.consoleLog("INFO", "subscriber info e"..subscriber_enterprise.."/s"..subscriber_site.."\n")

-- redis 
local redis = require 'redis'
local client = redis.connect('127.0.0.1', 6379)
local response = client:ping()           -- true
freeswitch.consoleLog("INFO", "connection to redis="..tostring(response).."\n")


fac_content= ""
if fac_action == "1" then
	fac_content= number
end
if fac_function == "01" then
	fac_name= "CFA"
elseif fac_function == "02" then
    fac_name= "CFB"
elseif  fac_function == "03" then
	fac_name= "CFNA"
elseif  fac_function == "04" then
	fac_name= "CFNR"	
else 
	freeswitch.consoleLog("WARNING", "Bad feature acces code: "..fac.."\n")
	fac_name="error"
end


key= "/bluebox/account/"..subscriber_number.."/"..fac_name

if fac_action == "1" then
	-- activate feature access code
	client:set(key, fac_content)
else 
	-- deactivate the feature access code
	client:del(key)
end

local value = client:get(key)      -- 
freeswitch.consoleLog("INFO", key..": "..tostring(value).."\n")

-- answer the call
session:answer()

-- sleep 2 second
session:sleep(2000)

-- play a file
-- session:streamFile("/usr/local/freeswitch/sounds/music/8000/suite-espanola-op-47-leyenda.wav");

-- hangup
session:hangup()
