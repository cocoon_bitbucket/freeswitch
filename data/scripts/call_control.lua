-- cc.lua
-- call control lua script
--

-- sofia/internal/+33146511101@192.168.1.50
dialB= "sofia/internal/+33146511102@192.168.1.50"


 dialA= "sofia/gateway/fs1/9903"
 -- dialB = "user/1001"    

-- session:answer();
-- while (session:ready() == true) do 
--   session:setAutoHangup(false);

    -- Initiate an outbound call

    -- obSession = freeswitch.Session(dialB)

    -- -- Check to see if the call was answered

    -- if obSession:ready() then
    --     -- Do something good here

    -- else    -- This means the call was not answered ... Check for the reason

    --     local obCause = obSession:hangupCause()

    --     freeswitch.consoleLog("info", "obSession:hangupCause() = " .. obCause )

    --     if ( obCause == "USER_BUSY" ) then              -- SIP 486
    --        -- For BUSY you may reschedule the call for later
    --     elseif ( obCause == "NO_ANSWER" ) then
    --        -- Call them back in an hour
    --     elseif ( obCause == "ORIGINATOR_CANCEL" ) then   -- SIP 487
    --        -- May need to check for network congestion or problems
    --     else
    --        -- Log these issues
    --     end
    -- end



--end 



 legA = freeswitch.Session(dialA)                                                     
 dispoA = "None"       
while(legA:ready() and dispoA ~= "ANSWER") do                 
 dispoA = legA:getVariable("endpoint_disposition")                                                 
 freeswitch.consoleLog("INFO","Leg A disposition is '" .. dispoA .. "'\n")
 os.execute("sleep 1")
end -- legA while
if( not legA:ready() ) then 
 -- oops, lost leg A handle this case 
 freeswitch.consoleLog("NOTICE","It appears that " .. dialA .. " disconnected...\n")
else 
 legB = freeswitch.Session(dialB) 
 dispoB = "None"
 while(legA:ready() and legB:ready() and dispoB ~= "ANSWER") do
   if ( not legA:ready() ) then
     -- oops, leg A hung up or got disconnected, handle this case
     freeswitch.consoleLog("NOTICE","It appears that " .. dialA .. " disconnected...\n")
   else
     os.execute("sleep 1")
     dispoB = legB:getVariable("endpoint_disposition")
     freeswitch.consoleLog("NOTICE","Leg B disposition is '" .. dispoB .. "'\n")
   end -- inner if legA ready
 end -- legB while   
 if ( legA:ready() and legB:ready() ) then
   freeswitch.bridge(legA,legB)
 else
   -- oops, one of the above legs hung up, handle this case
  freeswitch.consoleLog("NOTICE","It appears that " .. dialA .. " or " .. dialB .. " disconnected...\n")
 end
end -- outter if legA ready


