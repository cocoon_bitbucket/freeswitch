blueswitch configuration image
==============================

a configuration image for freeswith to test syprunner application




freeswitch configuration

conf/vars.xml
=============
customize: 
- <X-PRE-PROCESS cmd="set" data="external_rtp_ip=192.168.1.23"/>
- <X-PRE-PROCESS cmd="set" data="external_sip_ip=192.168.1.23"/>


conf/sip_profiles/internal.xml
===============================
- <param name="ext-rtp-ip" value="$${external_rtp_ip}"/>
- <param name="ext-sip-ip" value="$${external_sip_ip}"/>


conf/sip_profiles/external.xml
===============================
- <param name="ext-rtp-ip" value="$${external_rtp_ip}"/>
- <param name="ext-sip-ip" value="$${external_sip_ip}"/>


conf/autoload_configs/switch.conf.xml
======================================
-   <!-- RTP port range -->
-    <param name="rtp-start-port" value="16384"/> 
-    <param name="rtp-end-port"   value="16394"/> 


conf/dialplan/default.xml
=========================
 exclude the  Local_Extension_Skinny because of extension collision with 11?? bluebox extensions

