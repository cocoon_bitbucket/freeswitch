local server="192.168.1.51"

local redis = require 'redis'
local client = redis.connect(server, 6379)
local response = client:ping()           -- true
print ("connection to redis="..tostring(response).."\n")


local value = client:get('/bluebox/myvar') 
print ("/bluebox/myvar="..value.."\n")